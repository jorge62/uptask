const express = require('express');
const routes = require('./routes');
const path = require('path');
const helpers = require('./helpers');
const flash = require('connect-flash');
const session = require('express-session');
const cookieParser = require('cookie-parser'); 
const passport = require('./config/passport');


//crea conexion a bd
const db = require('./config/db');

//importar modelos
require('./models/Proyectos');
require('./models/Tareas');
require('./models/usuarios');   

db.sync()
    .then(() => console.log('conectado al servidor'))
    .catch((e) => console.log(e));

//crea un app de express
const app = express();

//habilitar archivos estaticos
app.use(express.static('public'));

//habilitar pug
app.set('view engine','pug');

//habilitar body parser
app.use(express.urlencoded({extended: true}));

//express-validator
//app.use(expressValidator());


//habilitar vistas
app.set('views',path.join(__dirname,'./views'));


// agregar flash messages
app.use(flash());

//habilitamos cookie-parser para auth de usuarios
app.use(cookieParser());

// habilitar sessiones para diferentes paginas sin volver a loguear
app.use(session({
    secret:'supersecreto',
    resave:false,
    saveUninitialized:false,
    resave : false,
    cookie: { sameSite: 'strict'},
}));

app.use(passport.initialize());
app.use(passport.session());

app.use((req,res,next)=>{
    res.locals.vardump = helpers.vardump;
    res.locals.mensajes = req.flash();
    res.locals.usuario = {...req.user} || null;
    next();
});



app.use('/',routes());

app.listen(3000);

