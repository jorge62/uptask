const Sequelize = require('sequelize');
const db = require('../config/db');
const Proyectos = require('./Proyectos');
const bcrypt = require('bcrypt-nodejs');
const passport = require('passport');

const Usuarios = db.define('usuarios',{
    id:{
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    email:{
        type: Sequelize.STRING(60),
        allowNull: false,
        validate:{
            isEmail:{
                msg:'Agrega un correo valido'
            },
            notEmpty:{
                msg:'Password no puede ir vacio'
            }    
        },
        unique:{
            args:true,
            msg:'Usuario ya registrado'
       }
    },
    password:{
        type: Sequelize.STRING(60),
        allowNull: true,
        validate:{
            notEmpty:{
                msg:'Password no puede ir vacio'
            }
        }
    },
    activo:{
        type: Sequelize.INTEGER,
        defaultValue:0
    },
    token: Sequelize.STRING,
    expiracion: Sequelize.DATE
},
{
    hooks:{
        beforeCreate(usuarios){
            usuarios.password = bcrypt.hashSync(usuarios.password, bcrypt.genSaltSync(10));
        }
    }
});


Usuarios.prototype.VerificarPassword = function(password){
    return bcrypt.compareSync(password,this.password);
}

Usuarios.hasMany(Proyectos);

module.exports = Usuarios;
  