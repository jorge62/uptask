const express= require('express');
//const { validator } = require('sequelize/types/lib/utils/validator-extras');
const router = express.Router();
const proyecto_controlador = require("../controllers/proyecto_controllers");
const tarea_controlador    = require('../controllers/tareas_controllers');
const usuarios_controlador = require('../controllers/usuario_controllers');
const sesion_controlador = require('../controllers/auth_controllers');
const { body } = require('express-validator/check');
//const { Router } = require('express');

module.exports=function(){
    router.get("/",sesion_controlador.RevisarAutenticacion,proyecto_controlador.proyectoshome);
    router.get("/nuevo-proyecto",sesion_controlador.RevisarAutenticacion,proyecto_controlador.proyectosnuevo);
    router.post("/nuevo-proyecto",
    body('nombre').not().isEmpty().trim().escape(),
    sesion_controlador.RevisarAutenticacion,proyecto_controlador.formularionuevo);
    router.get('/Misproyectos/:url',sesion_controlador.RevisarAutenticacion,proyecto_controlador.ProyectoPorURL);
    router.get('/Misproyectos/EditarProyecto/:id',sesion_controlador.RevisarAutenticacion,proyecto_controlador.EditarFormulario);
    router.post("/nuevo-proyecto/:id",
    body('nombre').not().isEmpty().trim().escape(),
    sesion_controlador.RevisarAutenticacion,proyecto_controlador.ActualizarProyecto);
    //eliminar proyecto
    router.delete('/Misproyectos/:url',sesion_controlador.RevisarAutenticacion,proyecto_controlador.eliminarproyecto);
    //tareas
    router.post('/Misproyectos/:url',sesion_controlador.RevisarAutenticacion,tarea_controlador.CrearTarea);
    // actualizar tareas
    router.patch('/tareas/:id',sesion_controlador.RevisarAutenticacion,tarea_controlador.ActualizarTarea);
    //eliminar tarea
    router.delete('/tareas/:id',sesion_controlador.RevisarAutenticacion,tarea_controlador.EliminarTarea);
    //crear cuenta usuarios
    router.get('/crear-Cuenta',usuarios_controlador.CrearFormUsuarios);
    //manejando POST de usuarios
    router.post('/crear-Cuenta',usuarios_controlador.CrearCuenta);
    //cambiar estado de cuenta
    router.get('/confirmar/:correo',usuarios_controlador.ConfirmarCuenta);
    //manejando Inicio de sesion
    router.get('/Iniciar-Sesion',usuarios_controlador.CrearFormSesion);
    //auth users
    router.post('/Iniciar-Sesion',sesion_controlador.AutenticarUsuario);  
    //cerrar sesion    
    router.get('/Cerrar-Sesion',sesion_controlador.CerrarSesion);
    //reestablecer password
    router.get('/reestablecer-password',usuarios_controlador.reestablecerPassword);
    // generar token
    router.post('/reestablecer-password',sesion_controlador.generarToken);
    // token get   
    router.get('/reestablecer/:token',sesion_controlador.validarToken);
    //actualizar password
    router.post('/reestablecer/:token',sesion_controlador.actualizarPassword);

    return router;
}

 //route para el home. Que se va a ver cuando entre a la pagina
// midleware,"funciones que se ejecutan una a otra"
