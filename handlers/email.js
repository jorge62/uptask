const nodemailer = require('nodemailer');
const pug = require('pug');
const juice = require('juice');
const htmlToText = require('html-to-text');
const util = require('util');
const emailConfig = require('../config/email');

let transporter = nodemailer.createTransport({
    host: emailConfig.host,
    port: emailConfig.port,
    secure: false, // true for 465, false for other ports
    auth: {
      user: emailConfig.user, // generated ethereal user
      pass: emailConfig.pass, // generated ethereal password
    },
});

// generar HTML

const generarHTML = (archivo,opciones={}) => {
  const html = pug.renderFile(`${__dirname}/../views/emails/${archivo}.pug`,opciones);
  return juice(html);
}

exports.enviar = async (opciones) => { 
  const html = generarHTML(opciones.archivo,opciones);
  const text = htmlToText.htmlToText(html);
  
  let opcionesMail = {
    from: '"UpTask 👻" <no-repley@uptask.com>', // sender address
    to: opciones.usuario.email,
    subject: opciones.subject,
    text,
    html 
  }

  const enviarMail = util.promisify(transporter.sendMail,transporter);
  return enviarMail.call(transporter,opcionesMail)

}