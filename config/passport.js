const passport = require("passport");

const LocalStrategy = require('passport-local');

// referencia al modelo donde vamos a autenticar
const Usuarios = require('../models/usuarios');


passport.use(
    new LocalStrategy(
        {
            usernameField:'email',
            passwordField:'password'
        },
        async (email,password,done) => {
            try {
                //si el user existe se almacena en const
                // de lo contrario salta al catch
                const usuario = await Usuarios.findOne({
                    where:{
                        email,
                        activo:1
                    }
                })
                //el user existe, password incorrecto
                if(!usuario.VerificarPassword(password)){
                    return done(null,false,{
                        message: 'Password incorrecto'
                    })
                }
                //el user existe, password es correcto
                return done(null,usuario);
            } catch (error) {
                // el user no existe
                return done(null,false,{
                    message: 'Esa cuenta no existe'
                })
            }
        }
    )
);


// serializar el usuario
passport.serializeUser((usuario,callback) => {
    callback(null,usuario);
});


//deserealizar el usuerio
passport.deserializeUser((usuario,callback) => {
    callback(null,usuario);
});


module.exports = passport;