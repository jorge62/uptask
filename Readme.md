![alt text](img/img2.png "load session") ![alt text](img/img1.png "tasks")

# UpTask - Node.js - Uptask project end code
This project is a task updater made with nodejs, sequilize and pug with the MVC model

## How to run this app

* Clone or download this repository.
   # **git clone https://gitlab.com/jorge62/uptask.git**
* Go to the repository's folder and open the terminal. type: 
   # **npm install**
* Lift mysql or mariadb service, in your terminal, type: 
   # **systemctl start mariadb or mysqld**
* Then in your terminal for run app, type:
   # **npm start**
* Open your web explorer and type: http://localhost:3000/ in the address bar. Done!

# Considerations
* Use table plus to manage the database, and mailtrap to track emails once the password recovery or email validation is required to register users.

