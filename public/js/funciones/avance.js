import Swal from 'sweetalert2';

export const actualizarAvance = () => {
    // listar tareas existentes
    const tareasExistentes = document.querySelectorAll('li.tarea');
    if(tareasExistentes.length){
    //listar tareas completadas
    const tareasCompletas = document.querySelectorAll('i.completo');
    //calcular tareas
    const avance = Math.round((tareasCompletas.length/tareasExistentes.length)*100);
    console.log(tareasCompletas.length);
    console.log(tareasExistentes.length);
    //mostrar avance
    const porcentaje = document.querySelector('#porcentaje');
    porcentaje.style.width = avance+'%';
    if(avance === 100){
        Swal.fire(
            'Tareas completatas',
            'Felicidades completaste las tareas',
            'success'
        )       
    }
    }   
}