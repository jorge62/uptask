import Swal from 'sweetalert2';
import axios from 'axios';

const btn = document.querySelector('#eliminar-proyecto');

if(btn){
btn.addEventListener('click',(e) => {
    const urlproyecto = e.target.dataset.proyectoUrl;
    
    Swal.fire({
        title: 'Estas segur@ ?',
        text: "Esto no tiene retorno!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancelado'  
    }).then((result) => {
        //enviar peticion con axios
          if(result.value){
              const url = `${location.origin}/Misproyectos/${urlproyecto}`;
              axios.delete(url,{ params: {urlproyecto }} )
                  .then(function(respuesta){
                        //console.log(respuesta)
                        Swal.fire(
                          'Borrado!',
                          'Su archivo ha sido borrado.',
                          'success'
                        );
                        setTimeout(()=>{
                         window.location.href='/';
                        },10000);   
                   })
                   .catch(()=>{
                     Swal.fire({
                      type:'error', 
                      title:'hubo un error',
                      text:'No se pudo eliminar el proyecto'
                     })
                   });
          }
      })
  
  })

}
export default btn;