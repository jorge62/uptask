import axios from "axios";
import Swal from 'sweetalert2';
import {actualizarAvance} from '../funciones/avance';

const tareas = document.querySelector('.listado-pendientes');

if(tareas){
    tareas.addEventListener('click', e => {
        if(e.target.classList.contains('fa-check-circle')){
            const icono = e.target;
            const idTarea = icono.parentElement.parentElement.dataset.tarea;
            console.log("aca id",idTarea);
            const url = `${location.origin}/tareas/${idTarea}`;
            axios.patch(url,{idTarea})
                .then(function(respuesta){
                    if(respuesta.status === 200){
                        icono.classList.toggle('completo');
                        //actualizando estado proyecto
                        actualizarAvance();
                    }
                })

        }
        const tareaHTML = e.target.parentElement.parentElement;
        const idTarea = tareaHTML.dataset.tarea;
        if(e.target.classList.contains('fa-trash')){
            Swal.fire({
                title: 'Deseas borrar la tarea ?',
                text: "Esto no tiene retorno !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar!',
                cancelButtonText: 'No, cancelado'  
            }).then((result)=>{
                if(result.value){
                    const url = `${location.origin}/tareas/${idTarea}`;
                    axios.delete(url,{params:{idTarea}})
                        .then(function(respuesta){
                            //eliminar nodo hijo
                            tareaHTML.parentElement.removeChild(tareaHTML);
                            //alerta
                            Swal.fire(
                                'Tarea eliminada',
                                'Su tarea ha sido eliminada',
                                'success'
                            )
                             //actualizando estado proyecto
                             actualizarAvance();
                        })
                }
            })    
        }
    });
}

export default tareas;