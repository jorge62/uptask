const webpack = require('webpack'); //to access built-in plugins
const path = require('path');

module.exports = {
    entry: './public/js/app.js',
    output: {
        path: path.resolve(__dirname, './public/dist'),
        filename: 'bundle.js',
    },
    watch:true,
    watchOptions: {
        poll: true,
        ignored: /node_modules/
      },
    module: {
        rules:  [
                    { 
                        test: /\.m?js$/, 
                        use: {
                            loader:'babel-loader',
                            options:{
                                    presets:['@babel/preset-env']
                            }
                        } 
                    }
                ],
    },
    
}

