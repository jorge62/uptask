const Usuarios = require("../models/usuarios");
const emailOpcion = require('../handlers/email')


exports.CrearFormUsuarios = (req,res) => {
    res.render('creando-cuenta',{
        nombrepagina: 'Login Acceso',
    });
}

exports.CrearFormSesion = (req,res) => {
    const {error} = res.locals.mensajes;

    res.render('Iniciar-Sesion',{
        nombrepagina: 'Iniciar Sesion en Uptask',
        error
    });
}

exports.CrearCuenta = async (req,res) => { 
    const {email, password} = req.body;
     
    try {
        await Usuarios.create({
            email,
            password
        });

        const ConfirmarURL = `http://${req.headers.host}/confirmar/${email}`;

        const usuario = {
            email
        }

        await emailOpcion.enviar({
            usuario,
            subject:"Confirma tu cuenta",
            ConfirmarURL,
            archivo:'confirmar-cuenta'
        })

        req.flash('correcto','confirma tu cuenta, te enviamos un email');
        res.redirect('/iniciar-sesion');
    }catch (error) {
        req.flash('error', error.errors.map(error=>error.message));
        res.render('creando-cuenta',{
            mensajes : req.flash(),
            email,
            password,
            nombrepagina: 'Crear cuenta en UpTask'
        })
    }
}

exports.reestablecerPassword = (req,res) => {
    res.render('reestablecer',{
        nomPagina:'Reestablecer tu password'
    })
}


exports.ConfirmarCuenta = async(req,res) => {
    const usuario = await Usuarios.findOne({
        where:{
            email:req.params.correo
        }
    })

    if(!usuario){
        req.flash('error','No es valido');
        res.redirect('crear-Cuenta');
    }

    usuario.activo = 1;
    await usuario.save();

    req.flash('correcto','Cuenta activada correctamente');
    res.redirect('/Iniciar-Sesion');

}