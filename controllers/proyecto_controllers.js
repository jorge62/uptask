const Proyecto = require('../models/Proyectos');
const Tareas = require('../models/Tareas');


exports.proyectoshome = async (req,res) => {
    const usuarioId = res.locals.usuario.id;
    const Misproyectos = await Proyecto.findAll({where:{usuarioId}});
    res.render("index",{
        nombrepagina : 'Proyectos',
        Misproyectos
    });
}

exports.proyectosnuevo = async (req,res) => {
    const usuarioId = res.locals.usuario.id;
    const Misproyectos = await Proyecto.findAll({where:{usuarioId}});

    res.render("nuevo-proyecto",{
        nombrepagina: 'Nuevo Proyecto',
        Misproyectos
    });
}

exports.formularionuevo = async (req,res)=> {

    const usuarioId = res.locals.usuario.id;
    const Misproyectos = await Proyecto.findAll({where:{usuarioId}});
   
   //valido campo nombre del body
    const {nombre} = req.body;
    
    let errores = [];

    if(!nombre){
        errores.push({
            'texto': 'agrega un nombre al proyecto'
        })
    }

    if(errores.length > 0){
        res.render('nuevo-proyecto',{
            nombrepagina : 'nuevo proyecto',
            errores,
            Misproyectos
        })
    }else{
        // no hay errores insertar en la BD
        const usuarioId = res.locals.usuario.id;
        const proyecto = await Proyecto.create({ nombre, usuarioId})
        res.redirect('/');
    }
}

exports.ProyectoPorURL = async (req,res,next)=>{
    
    const usuarioId = res.locals.usuario.id;
    const Misproyectos = await Proyecto.findAll({where:{usuarioId}});

    const proyecto = await Proyecto.findOne({
        where: {
            url: req.params.url,
            usuarioId
        }
    });

    //consultar tareas del proyecto
    const tareas = await Tareas.findAll({
        where:{
            proyectoId: proyecto.id
        }
    });

    if(!proyecto) return next();
    res.render('tareas',{
        nombre_tareas: 'tareas del proyecto',
        proyecto,
        Misproyectos,
        tareas
    })
   
}


exports.EditarFormulario = async (req,res) => {
    const usuarioId = res.locals.usuario.id;
    const MisproyectosPromise =  Proyecto.findAll({where:{usuarioId}});
    
    const proyectoPromise     = await Proyecto.findOne({
        where: {
            id: req.params.id,
            usuarioId
        }
    });

    const [Misproyectos,proyecto] = await Promise.all([MisproyectosPromise,proyectoPromise]);
    
    res.render('nuevo-proyecto',{
        nombrepagina: 'Editar Proyecto',
        Misproyectos,
        proyecto
    })
}

exports.ActualizarProyecto = async (req,res) => {

    const usuarioId = res.locals.usuario.id;
    const Misproyectos = await Proyecto.findAll({where:{usuarioId}});
    
    //valido campo nombre del body
    const {nombre} = req.body;
    
    let errores = [];

    if(!nombre){
        errores.push({
            'texto': 'agrega un nombre al proyecto'
        })
    }

    if(errores.length > 0){
        res.render('nuevo-proyecto',{
            nombrepagina : 'nuevo proyecto',
            errores,
            Misproyectos
        })
    }else{
        await Proyecto.update(
            { nombre : nombre},
            { where : { id : req.params.id }}
            )
        res.redirect('/');
    }
}    

exports.eliminarproyecto = async (req, res, next) => {
  const {urlproyecto} = req.query;
  const resultado = await Proyecto.destroy({where:{url:urlproyecto}});
  if(!resultado){
      return next();
  }
  res.status(200).send('Proyecto eliminado correctamente');
}