const Proyectos = require('../models/Proyectos');
const Tareas = require('../models/Tareas');

exports.CrearTarea = async (req,res,next) => {
    const proyecto = await Proyectos.findOne({where:{url:req.params.url}});
    // leer valor de tarea / ID / estado
    const {tarea} = req.body;
   // console.log(proyecto);
   // console.log("aca esta",proyecto.id);
    const proyectoId = proyecto.id;
    const estado = 0;

    const resultado = await Tareas.create({tarea, estado, proyectoId});
    if(!resultado){
        return next();
    }
    res.redirect(`/Misproyectos/${req.params.url}`)
    
 
}

exports.ActualizarTarea = async (req,res,next) => {
    const {id} = req.params;
    const tarea = await Tareas.findOne({where:{id}});

    // cambiar el estado
    let estado = 0;
    if(estado === tarea.estado){
        estado = 1;
    }
    tarea.estado = estado;

    const resultado = await tarea.save();

    if(!resultado)return next();
    res.status(200).send('todo bn');
}

exports.EliminarTarea = async (req,res,next) => {
    //destructuring
    const {id} = req.params;
    // eliminar tarea
    const resultado = await Tareas.destroy({where:{id}});
    if(!resultado){
        return next();
    }
    res.status(200).send('Eliminando');
}