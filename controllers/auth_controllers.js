const passport = require('passport');
const Usuario  = require('../models/usuarios');
const crypto   = require('crypto');
const Usuarios = require('../models/usuarios');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const bcrypt = require('bcrypt-nodejs');
const emailOpcion = require('../handlers/email');

exports.AutenticarUsuario = passport.authenticate('local',{
    successRedirect: '/',
    failureRedirect: '/Iniciar-Sesion',
    failureFlash: true,
    badRequestMessage: 'Ambos campos son obligatorios'
});

exports.RevisarAutenticacion = (req,res,next) =>{
    //si autenticado
    if(req.isAuthenticated()){
        return next();
    }

    //no autenticado
    return res.redirect('./Iniciar-Sesion');
}

exports.CerrarSesion = (req,res) => {
    req.session.destroy(()=>{
        res.redirect('/Iniciar-Sesion');
    });
}


exports.generarToken = async(req,res,next) => {

    // verificamos q exista el user
    const {email} = req.body;

    const usuario = await Usuario.findOne({where:{email}});

    // si no existe

    if(!usuario){
        req.flash('error','No existe esa cuenta');
        res.redirect('/reestablecer-password');
        return next()
    }

    // si existe el user
     usuario.token = crypto.randomBytes(20).toString('hex');
     usuario.expiracion = Date.now() + 360000;
     await usuario.save(); 

    const resetUrl = `http://${req.headers.host}/reestablecer/${usuario.token}`;
    console.log(resetUrl);

    //enviar el correo con el token
    await emailOpcion.enviar({
        usuario,
        subject:"Password reset",
        resetUrl,
        archivo:'resetear-password'
    })

    req.flash('correcto','Se envio un mail a tu correo')
    res.redirect('/Iniciar-Sesion')

}

exports.validarToken = async(req,res) => {
     const usuario = await Usuarios.findOne({
         where:{
             token:req.params.token
         }
     });

     if(!usuario){
         req.flash('error','No valido');
         res.redirect('/reestablecer-password');
     }

     res.render('resetpassword',{
         nomPagina: 'Reestablecer Contraseña'
     })
}

exports.actualizarPassword = async(req,res) => {
    const usuario = await Usuarios.findOne({
        where:{
            token:req.params.token,
            expiracion: {
                [Op.gte] : Date.now()
            }

        }
    });

    if(!usuario){
        req.flash('error','No valido');
        res.redirect('/reestablecer-password');    
    }

    usuario.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
    usuario.token = null;
    usuario.expiracion = null;

    await usuario.save();

    req.flash('correcto',' Tu password ha sido modificado correctamente');
    res.redirect('/Iniciar-sesion');
}

